//
//  example_trueid_communityApp.swift
//  example-trueid-community
//
//  Created by Surasit Intawong on 10/9/2564 BE.
//

import SwiftUI

@main
struct example_trueid_communityApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
