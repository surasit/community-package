//
//  CommunityClient.swift
//
//
//  Created by Surasit Intawong on 10/9/2564 BE.
//

import Foundation

public class CommunityClient {
    public static let shared = CommunityClient()
    private init() { }
    
    public func getCommunityVersion() -> String? {
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        return appVersion
    }
}
